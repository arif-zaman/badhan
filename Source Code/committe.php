<title> Committee </title>
<link rel="stylesheet" href="bootstrap.css"/>
<?php include('header.php');?>	
<?php include("databaseconnection.php"); ?>
<p>If you want to see central committee,Just press Submit. If you want to see a zonal committe,select the zone but dont select unit</p>
<form class="form-horizontal" action="committe.php" method="get">
<fieldset>

<!-- Form Name -->
<legend></legend>

<!-- Select Basic -->
<div class="form-group">
              <label class="col-md-4 control-label" for="zone">Zone</label>
              <div class="col-md-4">
                <select id="zone" name="zone"  class="form-control">
                <option value="..." <?php if (isset($_POST["zone"]) && $_POST["zone"] == "ZoneA") echo "selected='selected'";?> >...</option>
                  <option value="ZoneA" <?php if (isset($_POST["zone"]) && $_POST["zone"] == "ZoneA") echo "selected='selected'";?> >ZoneA</option>
                  <option value="ZoneB" <?php if (isset($_POST["zone"]) && $_POST["zone"] == "ZoneB") echo "selected='selected'";?> >ZoneB</option>
                  <option value="ZoneC" <?php if (isset($_POST["zone"]) && $_POST["zone"] == "ZoneC") echo "selected='selected'";?> >ZoneC</option>
                </select>
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-md-4 control-label" for="unit">Unit</label>
              <div class="col-md-4">
                <select id="unit" name="unit" class="form-control">
                  <option value="..." <?php if (isset($_POST["zone"]) && $_POST["zone"] == "ZoneA") echo "selected='selected'";?> >...</option>
                  <option value="UnitA" <?php if (isset($_POST["unit"]) && $_POST["unit"] == "UnitA") echo "selected='selected'";?>>UnitA</option>
                  <option value="UnitB" <?php if (isset($_POST["unit"]) && $_POST["unit"] == "UnitB") echo "selected='selected'";?> >UnitB</option>
                  <option value="UnitC" <?php if (isset($_POST["unit"]) && $_POST["unit"] == "UnitC") echo "selected='selected'";?> >UnitC</option>
                 
                </select>
              </div>
            </div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-4">
    <button id="submit" name="submit" class="btn btn-primary">Submit</button>
  </div>
</div>

</fieldset>
</form>

<?php
if(isset($_GET['zone']) && isset($_GET['unit']))
{
	$zone = strtoupper($_GET['zone']);
	$unit = strtoupper($_GET['unit']);
	if(!empty($zone) && !empty($unit))
	{
		if(($zone=="...") && ($unit=="..."))           //central
		{
			$query_central = "SELECT FIRSTNAME,LASTNAME,ZONE,DESIGNATIONNAME FROM MEMBER M,DESIGNATION D
	WHERE  M.DESIGNATIONID1=D.DESIGNATIONID ORDER BY D.DESIGNATIONID";
			$stid_central = oci_parse($conn,$query_central);
			if($query_run_central = oci_execute($stid_central))
			{
				$ncols = oci_num_fields($stid_central);
		
				echo "<h2 align=\"center\">Central Committee</h2>";
				echo "<table class=\"table\" align=\"center\"> <tr> \n";
				for ($i = 1; $i <= $ncols; ++$i) {
			 
					 $colname = oci_field_name($stid_central, $i);
					 echo "  <th><b>".htmlentities($colname, ENT_QUOTES)."</b></th>\n";
				 
				}
			echo "</tr>\n";
		//echo "</table>\n";
		
		//echo "<table class=\"table\"> <tr> \n";
		while ($row = oci_fetch_array($stid_central, OCI_ASSOC+OCI_RETURN_NULLS)) {
				echo "<tr>\n";
				
				foreach ($row as $item) {
					echo "<td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";
					
				}
				echo "</tr>\n";
		}
		echo "</table>\n";
			}
			else
			{
				
				
			}
		}
		else if ($unit=="...")                  //zone
		{
			$query_zonal = "SELECT FIRSTNAME,LASTNAME,UNIT,DESIGNATIONNAME FROM MEMBER M,DESIGNATION D
	WHERE M.ZONE = '$zone' AND  M.DESIGNATIONID2=D.DESIGNATIONID ORDER BY D.DESIGNATIONID";
			//echo $query_zonal;
			$stid_zonal = oci_parse($conn,$query_zonal);
			if($query_run_zonal = oci_execute($stid_zonal))
			{
				$ncols = oci_num_fields($stid_zonal);
		
				echo "<h2 align=\"center\">Zonal Committee,$zone Zone</h2>";
				echo "<table class=\"table\" align=\"center\"> <tr> \n";
				for ($i = 1; $i <= $ncols; ++$i) {
			 
					 $colname = oci_field_name($stid_zonal, $i);
					 echo "  <th><b>".htmlentities($colname, ENT_QUOTES)."</b></th>\n";
				 
				}
			echo "</tr>\n";
		//echo "</table>\n";
		
		//echo "<table class=\"table\"> <tr> \n";
		while ($row = oci_fetch_array($stid_zonal, OCI_ASSOC+OCI_RETURN_NULLS)) {
				echo "<tr>\n";
				
				foreach ($row as $item) {
					echo "<td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";
					
				}
				echo "</tr>\n";
		}
		echo "</table>\n";
				
			}
			
		}
		else if($zone!="..." && $unit!="...")         ///zone
		{
			$query_unit = "SELECT FIRSTNAME,LASTNAME,DESIGNATIONNAME FROM MEMBER M,DESIGNATION D
	WHERE (M.UNIT='$unit' AND M.ZONE='$zone') AND M.DESIGNATIONID3 <400 AND M.DESIGNATIONID3=D.DESIGNATIONID ORDER BY D.DESIGNATIONID";
	//echo $query_unit;
	//die();
			
			$stid_unit = oci_parse($conn,$query_unit);
			if($query_run_unit = oci_execute($stid_unit))
			{
				$ncols = oci_num_fields($stid_unit);
		
				echo "<h2 align=\"center\">Unit Committee,$unit ,$zone Zone</h2>";
				echo "<table class=\"table\" align=\"center\"> <tr> \n";
				for ($i = 1; $i <= $ncols; ++$i) {
			 
					 $colname = oci_field_name($stid_unit, $i);
					 echo "  <th><b>".htmlentities($colname, ENT_QUOTES)."</b></th>\n";
				 
				}
			echo "</tr>\n";
		//echo "</table>\n";
		
		//echo "<table class=\"table\"> <tr> \n";
		while ($row = oci_fetch_array($stid_unit, OCI_ASSOC+OCI_RETURN_NULLS)) {
				echo "<tr>\n";
				
				foreach ($row as $item) {
					echo "<td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";
					
				}
				echo "</tr>\n";
		}
		echo "</table>\n";
				
			}
			
			
			
		}
		
	}
	else 
	{
		
	}
	
}

?>
