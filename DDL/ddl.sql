create TABLE DONOR(
ID INT constraint DONORID primary key,
FIRSTNAME VARCHAR2(50),
LASTNAME VARCHAR2(50),
ZONE VARCHAR2(30),
UNIT VARCHAR2(30),
BLOODGROUP VARCHAR2(10),
DEPT VARCHAR2(15),
WILLINGTODONATE BOOL,
LASTDONATEDATE DATE,
CONTACTNO VARCHAR2(15),
DCOUNT INT
);

create TABLE STUDENT(
STUDENTID INT constraint S_ID primary key,
BATCH INT,
HALLROOM VARCHAR2(10),
ID INT 
);

create TABLE EMPLOYEE(
EMPLOYEEID INT constraint E_ID primary key,
JOBTITLE VARCHAR2(10),
ID INT
);

create TABLE MEMBER(
MEMBERID INT constraint M_ID primary key,
FIRSTNAME VARCHAR2(30),
LASTNAME VARCHAR2(30),
UNIT VARCHAR2(30),
ZONE VARCHAR2(30),
BLOODGROUP VARCHAR2(10),
CONTACTNO VARCHAR2(15)
);

create TABLE CENTRAL_MEMBER(
MEMBERID INT constraint M1_ID primary key,
DESIGNATION VARCHAR2(30)
);

create TABLE UNIT_MEMBER(
MEMBERID INT constraint M2_ID primary key,
DESIGNATION VARCHAR2(30)
);

create TABLE ZONE_MEMBER(
MEMBERID INT constraint M3_ID primary key,
DESIGNATION VARCHAR2(30)
);

create TABLE USER(
USERID INT constraint U_ID primary key,
HOSPITALID INT,
ID INT,
PATIENTID INT,
FIRSTNAME VARCHAR2(30),
LASTNAME VARCHAR2(30),
USERBLOODGROUP VARCHAR2(10),
TAKENBLOODGROUP VARCHAR2(10),
DONATIONOCCURANCE INT,
CONTACTNO VARCHAR2(15),
);

create TABLE ADDRESS(
USERID INT constraint U1_ID primary key,
DISTRICT VARCHAR2(30),
UPZILLA VARCHAR2(30),
VILLAGE VARCHAR2(30),
POSTOFFICE VARCHAR2(30),
HOUSE_NO VARCHAR2(10)
) ;

create TABLE HOSPITAL(
HOSPITALID INT constraint H_ID primary key,
NAME VARCHAR2(30),
LOCATION VARCHAR2(200),
PATIENTID INT
) ;

create TABLE INVENTORY(
INVENTORYID INT constraint I_ID primary key,
ID INT,
PATIENTID INT,
BLOODBAGID INT,
NAME VARCHAR2(30),
LOCATION VARCHAR2(200)
);

insert into DONOR(ID,FIRSTNAME,LASTNAME,ZONE,UNIT,BLOODGROUP,DEPT,WILLINGTODONATE,LASTDONATEDATE,CONTACTNO,DCOUNT) values
(1005031, 'ARIF', 'HASAN', 'BUET', 'SWH', 'A+', 'CSE', TRUE, '14-OCT-2013','01749017799',2);

insert into DONOR(ID,FIRSTNAME,LASTNAME,ZONE,UNIT,BLOODGROUP,DEPT,WILLINGTODONATE,LASTDONATEDATE,CONTACTNO,DCOUNT) values
(1005032, 'ALAMIN', 'HASAN', 'BUET', 'NIH', 'B+', 'CSE', TRUE, '20-OCT-2013','01749017799',3);

insert into DONOR(ID,FIRSTNAME,LASTNAME,ZONE,UNIT,BLOODGROUP,DEPT,WILLINGTODONATE,LASTDONATEDATE,CONTACTNO,DCOUNT) values
(1005033, 'RAJESH', 'KARMAKAR', 'JU', 'AAB', 'AB+', 'CE', TRUE, '14-NOV-2013','01479017799',5);

insert into DONOR(ID,FIRSTNAME,LASTNAME,ZONE,UNIT,BLOODGROUP,DEPT,WILLINGTODONATE,LASTDONATEDATE,CONTACTNO,DCOUNT) values
(1005054, 'ARIK', 'SARKER', 'BUET', 'TH', 'A-', 'WRE', TRUE, '14-DEC-2013','01749018899',6);

insert into DONOR(ID,FIRSTNAME,LASTNAME,ZONE,UNIT,BLOODGROUP,DEPT,WILLINGTODONATE,LASTDONATEDATE,CONTACTNO,DCOUNT) values
(1005058, 'NEEBIR', 'FARHAN', 'BUET', 'AH', 'O+', 'ME', TRUE, '14-OCT-2014','01749017788',4);


insert into STUDENT(STUDENTID,BATCH,HALLROOM,ID) values (31,10,4005,1005031);

insert into STUDENT(STUDENTID,BATCH,HALLROOM,ID) values (54,10,345,1005054);

insert into STUDENT(STUDENTID,BATCH,HALLROOM,ID) values (33,10,4009,1005033);

insert into STUDENT(STUDENTID,BATCH,HALLROOM,ID) values (58,10,315,1005058);

insert into STUDENT(STUDENTID,BATCH,HALLROOM,ID) values (32,10,3005,1005032);



insert into EMPLOYEE(EMPLOYEEID,JOBTITLE,ID) values (16,'OFFICER',1006016);

insert into EMPLOYEE(EMPLOYEEID,JOBTITLE,ID) values (24,'HALLGUARD',1006024);

insert into EMPLOYEE(EMPLOYEEID,JOBTITLE,ID) values (09,'DEAN',1006009);


insert into MEMBER(MEMBERID,FIRSTNAME,LASTNAME,UNIT,ZONE,BLOODGROUP,CONTACTNO) values 
(1008021,'RAIYAN','FERDOUS','SBH','BUET','AB+','01234567893');

insert into MEMBER(MEMBERID,FIRSTNAME,LASTNAME,UNIT,ZONE,BLOODGROUP,CONTACTNO) values 
(1005054,'ARIK','SARKER','AH','BUET','A+','04534567893');

insert into MEMBER(MEMBERID,FIRSTNAME,LASTNAME,UNIT,ZONE,BLOODGROUP,CONTACTNO) values 
(1006016,'SWARUP','KUMAR','SWH','BUET','B+','01234568793');

insert into MEMBER(MEMBERID,FIRSTNAME,LASTNAME,UNIT,ZONE,BLOODGROUP,CONTACTNO) values 
(1005021,'RAIYAN','KHAN','TH','BUET','A+','04434567893');

insert into MEMBER(MEMBERID,FIRSTNAME,LASTNAME,UNIT,ZONE,BLOODGROUP,CONTACTNO) values 
(1008021,'NIRAJ','FARHAN','SBH','BUET','AB-','01234566693');


insert into CENTRAL_MEMBER(MEMBERID,DESIGNATION) values (1005031,"GS");

insert into CENTRAL_MEMBER(MEMBERID,DESIGNATION) values (1005043,"VP");

insert into CENTRAL_MEMBER(MEMBERID,DESIGNATION) values (1005044,"FS");

insert into CENTRAL_MEMBER(MEMBERID,DESIGNATION) values (1005021,"GS");

insert into CENTRAL_MEMBER(MEMBERID,DESIGNATION) values (1005054,"P");


insert into UNIT_MEMBER(MEMBERID,DESIGNATION) values (1005031,"DONOR");

insert into UNIT_MEMBER(MEMBERID,DESIGNATION) values (1005032,"COLLECTOR");

insert into UNIT_MEMBER(MEMBERID,DESIGNATION) values (1005033,"MODERATOR");

insert into UNIT_MEMBER(MEMBERID,DESIGNATION) values (1005044,"UNITHEAD");

insert into UNIT_MEMBER(MEMBERID,DESIGNATION) values (1005054,"UNITHEAD");


insert into ZONE_MEMBER(MEMBERID,DESIGNATION) values (1005044,"ZONEHEAD");

insert into ZONE_MEMBER(MEMBERID,DESIGNATION) values (1005034,"ZONEPRESIDENT");

insert into ZONE_MEMBER(MEMBERID,DESIGNATION) values (1005033,"ZONEMODERATOR");

insert into ZONE_MEMBER(MEMBERID,DESIGNATION) values (1005050,"ZONEADVISOR");

insert into ZONE_MEMBER(MEMBERID,DESIGNATION) values (1005044,"ZONEHEAD");



insert into USER(USERID,HOSPITALID,ID,PATIENTID,FIRSTNAME,LASTNAME,USERBLOODGROUP,TAKENBLOODGROUP,DONATIONOCCURANCE,CONTACTNO) values
(1007060,2222,1005054,1222,'NAYEEM','ISLAM','A+','A-', 2, '01680040299');

insert into USER(USERID,HOSPITALID,ID,PATIENTID,FIRSTNAME,LASTNAME,USERBLOODGROUP,TAKENBLOODGROUP,DONATIONOCCURANCE,CONTACTNO) values
(1007059,2223,1005031,1222,'TURAB','ALI','O+','A+', 2, '01880040299');

insert into USER(USERID,HOSPITALID,ID,PATIENTID,FIRSTNAME,LASTNAME,USERBLOODGROUP,TAKENBLOODGROUP,DONATIONOCCURANCE,CONTACTNO) values
(1007058,2224,1005032,1223,'NEBIR','AHMED','A+','AB+', 2, '01689940299');

insert into USER(USERID,HOSPITALID,ID,PATIENTID,FIRSTNAME,LASTNAME,USERBLOODGROUP,TAKENBLOODGROUP,DONATIONOCCURANCE,CONTACTNO) values
(1007057,2225,1005034,1224,'SEZANA','ISLAM','A-','A+', 2, '01780040299');

insert into USER(USERID,HOSPITALID,ID,PATIENTID,FIRSTNAME,LASTNAME,USERBLOODGROUP,TAKENBLOODGROUP,DONATIONOCCURANCE,CONTACTNO) values
(1007067,2226,1005058,1225,'RAKIN','ISLAM','A+','O+', 4, '01608040299');


insert into ADDRESS(USERID,DISTRICT,UPZILLA,VILLAGE,POSTOFFICE,HOUSE_NO) values 
(1007070, 'DHAKA', 'RAMPURA', NULL, 'ABCD', '86');

insert into ADDRESS(USERID,DISTRICT,UPZILLA,VILLAGE,POSTOFFICE,HOUSE_NO) values 
(1007059, 'GAZIPUR', 'SRIPUR', 'CHOURIKAL', 'AFCD', '76');

insert into ADDRESS(USERID,DISTRICT,UPZILLA,VILLAGE,POSTOFFICE,HOUSE_NO) values 
(1007058, 'COMILLA', 'VOIRON', 'JALAM', 'BORURA', '96');

insert into ADDRESS(USERID,DISTRICT,UPZILLA,VILLAGE,POSTOFFICE,HOUSE_NO) values 
(1007057, 'GAZIPUR', 'SAPUR', 'CHOUL', 'AYCD', '56');

insert into ADDRESS(USERID,DISTRICT,UPZILLA,VILLAGE,POSTOFFICE,HOUSE_NO) values 
(1007067, 'LOKKHIPUR', 'TAPUR', 'CHIKAL', 'AVCD', '33');


insert into INVENTORY(INVENTORYID,ID,PATIENTID,BLOODBAGID,NAME,LOCATION) values 
(999,1005054,1225,777,'QUANTAM','DHAKA MEDICAL COLLEGE');

insert into INVENTORY(INVENTORYID,ID,PATIENTID,BLOODBAGID,NAME,LOCATION) values 
(998,1005033,1224,776,'QUANTAM','BOGURA MEDICAL COLLEGE');

insert into INVENTORY(INVENTORYID,ID,PATIENTID,BLOODBAGID,NAME,LOCATION) values 
(997,1005031,1223,775,'SONDHANI','AL-RAZI HOSPITAL DHAKA');

insert into INVENTORY(INVENTORYID,ID,PATIENTID,BLOODBAGID,NAME,LOCATION) values 
(996,1005058,1227,757,'BADHAN','METROPOPLITAN HOSPITAL DHAKA');

insert into INVENTORY(INVENTORYID,ID,PATIENTID,BLOODBAGID,NAME,LOCATION) values 
(995,1005032,1222,737,'QUANTAM','DHAKA PG HOSPITAL');


insert into HOSPITAL (HOSPITALID, HOSPITALNAME, LOCATION, PATIENTID) values
(2222, 'DHAKA MEDICAL COLLEGE', 'DHAKA', 1225);

insert into HOSPITAL (HOSPITALID, HOSPITALNAME, LOCATION, PATIENTID) values
(2223, 'BOGURA MEDICAL COLLEGE', 'BOGURA', 1224);

insert into HOSPITAL (HOSPITALID, HOSPITALNAME, LOCATION, PATIENTID) values
(2224, 'AL-RAZI HOSPITAL DHAKA', 'DHAKA', 1223);

insert into HOSPITAL (HOSPITALID, HOSPITALNAME, LOCATION, PATIENTID) values
(2225, 'METROPOPLITAN HOSPITAL DHAKA', 'DHAKA', 1227);

insert into HOSPITAL (HOSPITALID, HOSPITALNAME, LOCATION, PATIENTID) values
(2226, 'DHAKA PG HOSPITAL', 'DHAKA', 1229);


alter TABLE ADDRESS add DIVISION varchar2(20);

insert into ADDRESS(USERID,DISTRICT,UPZILLA,VILLAGE,POSTOFFICE,HOUSE_NO,DIVSION) values 
(1007070, 'DHAKA', 'RAMPURA', NULL, 'ABCD', '86', 'DHAKA');

insert into ADDRESS(USERID,DISTRICT,UPZILLA,VILLAGE,POSTOFFICE,HOUSE_NO,DIVISION) values 
(1007079, 'GAZIPUR', 'SRIPUR', 'CHOURIKAL', 'AFCD', '76','DHAKA');

insert into ADDRESS(USERID,DISTRICT,UPZILLA,VILLAGE,POSTOFFICE,HOUSE_NO,DIVISION) values 
(1007058, 'COMILLA', 'VOIRON', 'JALAM', 'BORURA', '96','CHITTAGONG');

insert into ADDRESS(USERID,DISTRICT,UPZILLA,VILLAGE,POSTOFFICE,HOUSE_NO,DIVISION) values 
(1007057, 'KUSTIA', 'SAPUR', 'CHOUL', 'AYCD', '56','RAJSHAHI');

insert into ADDRESS(USERID,DISTRICT,UPZILLA,VILLAGE,POSTOFFICE,HOUSE_NO,DIVISION) values 
(1007056, 'LOKKHIPUR', 'TAPUR', 'CHIKAL', 'AVCD', '33','CHITTAGONG');
